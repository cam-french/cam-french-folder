# Tunebook template

This template is for a [SvelteKit](https://kit.svelte.dev/) static site, making heavy use of the components from [@flyingcatband/tunebook](https://www.npmjs.com/package/@flyingcatband/tunebook). It is designed to make a simple viewer for your (folk dance) band to see their collection of tunes, pick an appropriate tune or set of tunes to play, view the sheet music, and transpose if required.

## Development

Development is pretty standard for SvelteKit projects - you'll need a javascript runtime (probably NodeJS) and a package manager (`npm` or `pnpm`) to install the dependencies. There's a `justfile` for convenience if you use `pnpm`. There's some static analysis and a formatter configured. There's no tests, because most of the logic is handles by the components in their package, and that has tests.

It's a modern JavaScript project, so most of the files in the root are configuration for various tools, which you shouldn't need to touch. Static assets (favicon, PDFs) live in `/static`, your tunes live in `/data`, the website routing and pages are defined in `/src/routes`. Anything else can broadly be ignored.

## Data

There's a directory called `data`. This is where your tunes go, in [abc format](https://abcnotation.com/wiki/abc:standard). There's a few example tunes in there now, delete them! If you define your folder in LaTeX, put the `tex` file in there too. Update `src/folder.json/server.ts` to point to a file with the right name, and use an appropriate function to parse that file into a format that the tunebook components can read.

## Design

This template uses TailwindCSS for convenience, feel free to remove it if you don't like it.

If you want a custom font (you probably do), install it from [fontsource](https://fontsource.org/), or however you like to get fonts from a CDN in your CSS. Search the codebase for `My Custom Font`, and replace that string with the name of your font.

If you don't want a custom font, search the codebase for `My Custom Font` and remove those references.

TODO Favicon, and icons for manifest.json when we do #2, as #3

At some point, we'll automate this, but for now, find and replace is your friend.

## Deployment

By default, this template makes a site that search engines can't index. This means you won't be able to Google for your own folder, you'll have to go straight to it's web address. This is defined in `src/app.html` if you really want to change it, but think carefully about the possibility of sharing copyrighted data on the open internet.

You can deploy the site anywhere you can deploy static sites. We like [Cloudflare pages](https://pages.cloudflare.com/), but Netlify, Vercel, Render or a raspberry pi running a file server would all work fine.

## Generate a PDF version

Coming soon...


## Questions
- [ ] Can we normalise the tunes into how we write them out? Eg all schottishes are written in 2/2 with quavers, all 3-time bourrees in 3/8
- [ ] anything with 1,3/2,4 repeats can we just write it out? Are you more concerned with saving paper or making tunes easier to scan
- [ ] the things that have been transposed into keys that are nice for gurdy but rubbish for wind and d/g box, can we move them back? Things "originally in G" etc. Likewise for moving things that just don't fit nicely (Paddy's tricot etc)
- [ ] can we break tunes into really common/kinda common/never play, to help people know what to pratice
- [ ] the tune called "new french schottishe" has a known name (mominette) and composer, and should be in a different key, can we fix it?
- [ ] can we decide how to spell sc(h)ottis(che)?
- [ ] serpentiner och konfetti is a bad transcribe and a terrible key for basically everyone, can i fix it
- [ ] is sunshine definitely that way round?
- [ ] is it worth organising weird-beat waltzes separate from 3-beat waltzes
- [ ] which ABC fields should be rendered on the online book? Do you want to see book, discog, transcriber?
- [ ] there's 2 copies of Chypre in 2 keys, do you want to keep both/either?