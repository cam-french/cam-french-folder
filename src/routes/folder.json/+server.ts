import slugify from 'slugify';
import type { Folder, Section } from '@flyingcatband/tunebook/types';
import { json } from '@sveltejs/kit';
import { readFile } from 'fs/promises';

export const prerender = true;

export function GET(): Promise<Response> {
	return generateFolderFromCamFrenchAbc().then(json);
}

async function generateFolderFromCamFrenchAbc(): Promise<Folder> {
	const abc = await readFile('./data/cam-french.abc').then((buf) => buf.toString());
	const folder: Folder = {
		name: 'The Cam-French tune collection',
		content: []
	};
	let currentAbc = '';
	let setTitle: string | undefined = undefined;
	let currentSection: Section;
	let sectionTitle: string | undefined = undefined;
	const pushCurrentTune = (title: string) => {
		if (!sectionTitle)
			throw Error(
				'Not currently in a section. Expected a `%%center Section Title Here` before the first ABC document'
			);

		let section = currentSection
		if (currentSection?.name != sectionTitle) {
			currentSection = {
				name: sectionTitle,
				content: []
			};
			folder.content.push(currentSection);
		}
		section ??= currentSection;
		section.content.push({
			slug: slugify(title, { strict: true }),
			name: title,
			notes: [],
			content: [
				{
					filename: '',
					slug: slugify(title, { strict: true }),
					abc: currentAbc,
				}
			]
		});
	};
	for (const line of abc.split('\n')) {
		if (line.match(/X: *[0-9]+/)) {
			if (currentAbc && setTitle) {
				pushCurrentTune(setTitle);
			}
			setTitle = undefined;
			currentAbc = '';
		}
		
		const abcTitle = line.match(/T: *(.+)/)?.[1];
		const newSectionTitle = line.match(/%%center +(.+)/)?.[1];
		if (newSectionTitle && newSectionTitle != sectionTitle) {
			sectionTitle = newSectionTitle;
		}
		if (abcTitle && !setTitle) {
			setTitle = abcTitle;
		}

		currentAbc += line;
		currentAbc += '\n';
	}
	if (currentAbc && setTitle) {
		pushCurrentTune(setTitle);
	}
	return folder;
}
