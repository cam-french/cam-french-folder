#!/usr/bin/env bash


# Generate PS file from abc file
abcm2ps cam-french.abc -O =

# Add an index to the PS file
tclsh abcindex.tcl cam-french.ps cam-french-index.ps

# convert to PDF
ps2pdf cam-french-index.ps cam-french.pdf

# add bookmarks to PDF
pdftk cam-french.pdf update_info cam-french-index.info output cam-french-bookmarked.pdf
mv cam-french-bookmarked.pdf cam-french.pdf

# BFLAT HACK
# Transpose without checks
abc2abc cam-french.abc -r -b -t 2 > cam-french-bflat.abc

# Generate PS file from abc file
abcm2ps cam-french-bflat.abc -O =

# Add an index to the PS file
tclsh abcindex.tcl cam-french-bflat.ps cam-french-bflat-index.ps

# convert to PDF
ps2pdf cam-french-bflat-index.ps cam-french-bflat.pdf

# add bookmarks to PDF
pdftk cam-french-bflat.pdf update_info cam-french-bflat-index.info output cam-french-bflat-bookmarked.pdf
mv cam-french-bflat-bookmarked.pdf cam-french-bflat.pdf